package com.daniel.tienda;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by daniel on 19/01/2017.
 */
@Entity
public class Carritos {
    private int id;
    private Timestamp fechaCreacion;
    private Float precioTotal;
    private Boolean finalizado;
    private List<CarritoProducto> producto;
    private Usuarios usuario;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Timestamp getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Timestamp fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Basic
    @Column(name = "precio_total")
    public Float getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Float precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Basic
    @Column(name = "finalizado")
    public Boolean getFinalizado() {
        return finalizado;
    }

    public void setFinalizado(Boolean finalizado) {
        this.finalizado = finalizado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Carritos carritos = (Carritos) o;

        if (id != carritos.id) return false;
        if (fechaCreacion != null ? !fechaCreacion.equals(carritos.fechaCreacion) : carritos.fechaCreacion != null)
            return false;
        if (precioTotal != null ? !precioTotal.equals(carritos.precioTotal) : carritos.precioTotal != null)
            return false;
        if (finalizado != null ? !finalizado.equals(carritos.finalizado) : carritos.finalizado != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (fechaCreacion != null ? fechaCreacion.hashCode() : 0);
        result = 31 * result + (precioTotal != null ? precioTotal.hashCode() : 0);
        result = 31 * result + (finalizado != null ? finalizado.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "carrito")
    public List<CarritoProducto> getProducto() {
        return producto;
    }

    public void setProducto(List<CarritoProducto> producto) {
        this.producto = producto;
    }

    @OneToOne(mappedBy = "carrito")
    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }
}
