package com.daniel.tienda;

import javax.persistence.*;

/**
 * Created by daniel on 19/01/2017.
 */
@Entity
@Table(name = "carrito_producto", schema = "ejercicio8", catalog = "")
public class CarritoProducto {
    private Float precio;
    private int cantidad;
    private int id;
    private Carritos carrito;

    @Basic
    @Column(name = "precio")
    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarritoProducto that = (CarritoProducto) o;

        if (cantidad != that.cantidad) return false;
        if (id != that.id) return false;
        if (precio != null ? !precio.equals(that.precio) : that.precio != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = precio != null ? precio.hashCode() : 0;
        result = 31 * result + cantidad;
        result = 31 * result + id;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_carrito", referencedColumnName = "id", nullable = false)
    public Carritos getCarrito() {
        return carrito;
    }

    public void setCarrito(Carritos carrito) {
        this.carrito = carrito;
    }
}
