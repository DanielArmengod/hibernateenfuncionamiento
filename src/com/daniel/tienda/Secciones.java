package com.daniel.tienda;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * Created by daniel on 19/01/2017.
 */
@Entity
public class Secciones {
    private int id;
    private String codigo;
    private String nombre;
    private String descripcion;
    private Date fechaCreacion;
    private List<Productos> productos;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Secciones secciones = (Secciones) o;

        if (id != secciones.id) return false;
        if (codigo != null ? !codigo.equals(secciones.codigo) : secciones.codigo != null) return false;
        if (nombre != null ? !nombre.equals(secciones.nombre) : secciones.nombre != null) return false;
        if (descripcion != null ? !descripcion.equals(secciones.descripcion) : secciones.descripcion != null)
            return false;
        if (fechaCreacion != null ? !fechaCreacion.equals(secciones.fechaCreacion) : secciones.fechaCreacion != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (codigo != null ? codigo.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (fechaCreacion != null ? fechaCreacion.hashCode() : 0);
        return result;
    }

    @ManyToMany
    @JoinTable(name = "seccion_producto", catalog = "", schema = "ejercicio8", joinColumns = @JoinColumn(name = "id_seccion", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false))
    public List<Productos> getProductos() {
        return productos;
    }

    public void setProductos(List<Productos> productos) {
        this.productos = productos;
    }
}
