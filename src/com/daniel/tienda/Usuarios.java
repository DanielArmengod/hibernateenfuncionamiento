package com.daniel.tienda;

import javax.persistence.*;
import java.util.List;

/**
 * Created by daniel on 19/01/2017.
 */
@Entity
public class Usuarios {
    private int id;
    private String login;
    private String password;
    private String nif;
    private String email;
    private String nombre;
    private String direccion;
    private String telefono;
    private List<UsuarioSeccion> seccion;
    private Carritos carrito;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "nif")
    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Usuarios usuarios = (Usuarios) o;

        if (id != usuarios.id) return false;
        if (login != null ? !login.equals(usuarios.login) : usuarios.login != null) return false;
        if (password != null ? !password.equals(usuarios.password) : usuarios.password != null) return false;
        if (nif != null ? !nif.equals(usuarios.nif) : usuarios.nif != null) return false;
        if (email != null ? !email.equals(usuarios.email) : usuarios.email != null) return false;
        if (nombre != null ? !nombre.equals(usuarios.nombre) : usuarios.nombre != null) return false;
        if (direccion != null ? !direccion.equals(usuarios.direccion) : usuarios.direccion != null) return false;
        if (telefono != null ? !telefono.equals(usuarios.telefono) : usuarios.telefono != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (nif != null ? nif.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (direccion != null ? direccion.hashCode() : 0);
        result = 31 * result + (telefono != null ? telefono.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "usuario")
    public List<UsuarioSeccion> getSeccion() {
        return seccion;
    }

    public void setSeccion(List<UsuarioSeccion> seccion) {
        this.seccion = seccion;
    }

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id_usuario", nullable = false)
    public Carritos getCarrito() {
        return carrito;
    }

    public void setCarrito(Carritos carrito) {
        this.carrito = carrito;
    }
}
