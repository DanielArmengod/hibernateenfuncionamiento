package com.daniel.tienda;

import javax.persistence.*;
import java.util.List;

/**
 * Created by daniel on 19/01/2017.
 */
@Entity
public class Productos {
    private int id;
    private String codigo;
    private String nombre;
    private String descripcion;
    private Float precio;
    private List<Secciones> secciones;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "precio")
    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Productos productos = (Productos) o;

        if (id != productos.id) return false;
        if (codigo != null ? !codigo.equals(productos.codigo) : productos.codigo != null) return false;
        if (nombre != null ? !nombre.equals(productos.nombre) : productos.nombre != null) return false;
        if (descripcion != null ? !descripcion.equals(productos.descripcion) : productos.descripcion != null)
            return false;
        if (precio != null ? !precio.equals(productos.precio) : productos.precio != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (codigo != null ? codigo.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (precio != null ? precio.hashCode() : 0);
        return result;
    }

    @ManyToMany(mappedBy = "productos")
    public List<Secciones> getSecciones() {
        return secciones;
    }

    public void setSecciones(List<Secciones> secciones) {
        this.secciones = secciones;
    }
}
