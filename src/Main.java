import com.daniel.tienda.Usuarios;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.SessionFactoryBuilder;
import org.hibernate.boot.spi.SessionFactoryBuilderFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.context.spi.CurrentSessionContext;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.sql.Connection;
import java.util.Map;

/**
 * Created by daniel on 17/01/2017.
 */
public class Main {
    public Main(){
        HibernateUtil.buildSessionFactory();
        Usuarios usuario = new Usuarios();
        usuario.setLogin("asdas");
        usuario.setPassword("asd");
        usuario.setNif("asdasd");
        usuario.setEmail("ASDASD");
        usuario.setNombre("asdasd");
        usuario.setDireccion("asdasd");
        usuario.setTelefono("5465466");
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(usuario);
        sesion.getTransaction().commit();
        sesion.close();
    }
    public static void main(String [] args){
        new Main();
    }
}